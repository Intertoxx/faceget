#!/usr/bin/env python
# encoding: utf-8
import facebook
import sqlite3
import webbrowser
import Database

webbrowser.open('https://developers.facebook.com/tools/explorer/272230902814914')
token = str(raw_input("Entrez le token facebook : "))
conn = sqlite3.connect('facefinder.db')
cursor = conn.cursor()
graph = facebook.GraphAPI(token)
profile = graph.get_object("me/friends")
amiss = graph.get_object("me/taggable_friends",**{'limit':9999,'fields':'first_name,last_name,id,picture.type(large)'})
tot_friend = float(profile['summary']['total_count'])

for (i, item) in enumerate(amiss['data']):
	percent = float((i/tot_friend)*100)
	print repr(float("%3.f" %percent)) + " %"
	cursor.execute("""INSERT OR IGNORE INTO friends(prenom,nom,photo,fbid) VALUES(?,?,?,?)""",(item['first_name'],item['last_name'],item['picture']['data']['url'],item['id']))
	conn.commit()
print("Ajout des personnes terminee")