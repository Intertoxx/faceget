import sqlite3

conn = sqlite3.connect('facefinder.db')
amis = conn.cursor()
amis.execute("""
	CREATE TABLE IF NOT EXISTS friends(
		id INTEGER,
		prenom TEXT UNIQUE,
		nom TEXT PRIMARY KEY UNIQUE,
		photo TEXT UNIQUE,
		fbid TEXT
		)
""")
conn.commit()